<li>
    <h2>{{$list_item['Name']}}

    (
        @if($list_item['child_count']>$list_item['total_item'])
            {{$list_item['child_count']}}
        @else
            {{$list_item['total_item']}}
        @endif
    )

    </h2>

    @if ($list_item['child_count']>0)
        <ul>
            @each('list_item', $list_item['child_items'], 'list_item')
        </ul>
    @endif
</li>

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repo;

///
use App\Repositories\RepoInterface;

class RepoController extends Controller
{
    //////////////////////
    /// I have used repository pattern to separate DAL from the business logic.
    /// In order to to that I have created a RepoInterface as RepoClass and also bind them using a ServiceProvider
    //////////////////////

    protected $repodal;
    public function __construct(RepoInterface $repodal){
        $this->repodal = $repodal;
    }
    /////// Task 1 /////////
    public function task1(){
        //////////////////
        /// @params: null
        /// returns view
        /// description: This function returns a view for task 1
        //////////////////

        $data = $this->item_count_data();
        return view('task1',compact('data'));
    }
    //
    public function item_count_data(){
        //////////////////
        /// @params: null
        /// returns data array
        /// description: This function returns data array for task 1
        //////////////////

        return $this->repodal->get_category_item_count();
    }
    //////// Task 2 /////////
    public function task2(){
        //////////////////
        /// @params: null
        /// returns view
        /// description: This function returns a view for task 2
        //////////////////

        $category_list = $this->get_category_list();
        return view('task2',compact('category_list'));
    }

    public function get_category_list(){
        //////////////////
        /// @params: null
        /// returns data array
        /// description: This function returns data array for task 2
        //////////////////

        $category_list = $this->repodal->category_tree();
        return $category_list;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategoryRelations extends Model
{
    //
    protected $table = 'item_category_relations';
}

<?php
namespace App\Repositories;

interface RepoInterface{
    public function get_category_item_count();
    public function parent_category_list();
    public function child_category_list($parent_id,$level);
    public function category_tree();
    public function get_child_total_item($parent);
}
?>

<?php
namespace App\Repositories;
use App\Category;
class RepoClass implements RepoInterface{
    public function get_category_item_count(){
        //////////////////
        /// @params: null
        /// returns data array
        /// description: This function returns number of total item per category name
        //////////////////
        $query = "SELECT
                ct.Name as category_name,
                COUNT(it.Id) as total_item
                FROM ecommerce_db.category ct
                JOIN ecommerce_db.Item_category_relations icr ON icr.categoryId = ct.Id
                JOIN ecommerce_db.Item it ON icr.ItemNumber = it.Number
                GROUP BY ct.Name
                ORDER BY COUNT(it.Id) DESC";

        $data = \DB::select(\DB::raw($query));
        return $data;
    }
    public function parent_category_list(){
        //////////////////
        /// @params: null
        /// returns data array
        /// description: This function returns a list of parent categories
        //////////////////
        $query = "SELECT
                ct.Name,
                cr.ParentcategoryId
                FROM ecommerce_db.catetory_relations cr
                JOIN ecommerce_db.category ct ON cr.ParentcategoryId = ct.id";

        $data = \DB::select(\DB::raw($query));
        return $data;
    }

    public function child_category_list($parent_id,$level){
        //////////////////
        /// @params: $parent_id(int)
        /// returns data array
        /// description: This function returns a list of child categories for a parent
        //////////////////
        $query = "SELECT
                cr.categoryId,
                ct.Name
                FROM ecommerce_db.catetory_relations cr
                JOIN category ct ON cr.categoryId = ct.Id
                WHERE cr.ParentcategoryId = :id";

        $data = \DB::select(\DB::raw($query),array('id'=>$parent_id));
        $count = count($data);
        $subtree = array();
        $level = $level+1;
        if($count>0){
            foreach($data as $row){
                $temp_row = array();
                $children = $this->child_category_list($row->categoryId,$level);
                $temp_row["level"] = $level;
                $temp_row["Name"] = $row->Name;
                $temp_row["child_count"] = count($children);
                $temp_row["child_items"] = $children;
                $temp_row = $this->get_child_total_item($temp_row);
                array_push($subtree,$temp_row);
            }
        }
        return $subtree;
    }

    public function category_tree(){
        //////////////////
        /// @params: null
        /// returns data array
        /// description: This function returns a category tree
        //////////////////
        $category_tree = array();
        $parent_list = $this->parent_category_list();
        if(count($parent_list)>0){
            foreach($parent_list as $row){
                $parent_id = $row->ParentcategoryId;
                $temp_row = array();
                $level = 0;

                $children = $this->child_category_list($parent_id,$level);
                $temp_row["level"] = $level;
                $temp_row["Name"] = $row->Name;
                $temp_row["child_count"] = count($children);
                $temp_row["child_items"] = $children;
                $temp_row = $this->get_child_total_item($temp_row);

                array_push($category_tree,$temp_row);
            }
        }


        return $category_tree;
    }

    public function get_child_total_item($parent){
        if(count($parent)>0){
            $items = 0;
            if($parent["child_count"]>0){
                foreach($parent["child_items"] as $row){
                    $items = $items + $row["child_count"];
                    $child = $this->get_child_total_item($row);
                }
            }
            $parent["total_item"] = $items;
            return $parent;
        }
    }

}


?>
